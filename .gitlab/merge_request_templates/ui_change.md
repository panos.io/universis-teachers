
## What does this MR do?
Add a description of your merge request.
- What are you implementing?
- What are you trying to achieve with these changes?

## Related issues
Add a reference to issues related with this merge request.

If there are no related issues:
- Describe the problem
- Provide steps to reproduce

## Link(s) to related invision screen(s)
  * Add link
  * Add link

## Developer Checklist
- [ ] My branch is up-to-date with the upstream `master` branch
- [ ] Coding is in progress, and I have marked the MR as WIP
- [ ] Coding is completed and the MR is ready for review
- [ ] My MR follows the [contribution guidelines](https://gitlab.com/universis/universis-teachers/blob/master/CONTRIBUTING.md)
- [ ] I have successfully run the code of this merge request locally
- [ ] I have verified locally that my changes work for all necessary screen sizes
- [ ] I have added a comment with **screenshots** of the code running locally

## Tech review Checklist
Have you verified that what is supposed to happen, actually does, and what is not supposed to happen, indeed does not?

- [ ] The MR accurately **describes** the changes and has a relevant title/description
- [ ] The MR **does what it is supposed to** according to its *title*, *description* and related *issues*/*links*
- [ ] I have successfully run the changes locally, and tried the new code

## MR review Checklist
- [ ] The MR references related issues/MRs
- [ ] The MR provides links to screens and screenshots
- [ ] The **commits** of the MR describe the changes, have proper wording, and follow the [guidelines](https://gitlab.com/universis/universis-students/blob/master/CONTRIBUTING.md#style)
- [ ] I have successfully run the changes locally, and tried the new code
- [ ] The MR is **ready for merge** (rebased, commit squashed if needed, etc)
