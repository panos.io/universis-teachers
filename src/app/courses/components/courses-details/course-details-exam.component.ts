import { Component, OnInit, Input } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {CoursesService} from '../../services/courses.service';
import {CoursesSharedService} from '../../services/courses-shared.service';
import {ErrorService} from '../../../error/error.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {LoadingService} from '../../../shared/services/loading.service';
import {GradeScaleService} from '../../services/grade-scale.service';

@Component({
  selector: 'app-courses-details-exam',
  templateUrl: './course-details-exam.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsExamComponent implements OnInit {

  public courseExam: any;
  public uploadHistory: any;
  public subDate: any;

  public studentGradesHistory: any;   // for Chart and Statistics
  public studentsRegisters = 0;
  public studentsGraded = 0;
  public studentsSuccessed = 0;

  public isLoading = true;

  // Charts values
  public chartGradesOptions: any;
  public chartGradesLabels: string[] ;
  public chartGradesData: any[] ;
  public chartGradesType = 'bar';
  public chartGradesLegend = true;
  public chartGradeColours: Array<any> = [
    { // blue
      backgroundColor: 'rgba(0,98,201,1)',
      borderColor: 'rgba(0,98,201,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private  coursesService: CoursesService,
              private coursesSharedService: CoursesSharedService,
              private  errorService: ErrorService,
              private router: Router,
              private route: ActivatedRoute,
              private loadingService: LoadingService,
              private gradeScaleService: GradeScaleService) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.loadingService.showLoading();
    this.route.params.subscribe(routeParams => {
      this.coursesService.getCourseExam(routeParams.courseExam).then((result) => {
        this.courseExam = result;
        this.coursesService.getUploadHistory(this.courseExam.id).then((value) => {
          this.uploadHistory = value.value;
          if (this.uploadHistory && this.uploadHistory.length > 0) {

            this.subDate = this.uploadHistory[0].result.dateCreated;
          }
          this.loadingService.hideLoading();
        }).catch(err => {
          return this.errorService.navigateToError(err);
        });

        this.loadStatistics();
      }).catch(err => {
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    });
  }

  importGrades(courseExam) {
      this.coursesSharedService.importCourseExamGradesDialog(courseExam).then(res => {
        if (res === true ) {
          this.loadData();  // reload page data
        }
     });
  }

  loadStatistics() {
    this.gradeScaleService.getGradeScale(this.courseExam.course.gradeScale).then( gradeScale => {
      this.coursesService.getGradesStatistics(this.courseExam.id).then((result) => {
        this.studentGradesHistory = result.value;

        // get registered students
        this.studentsRegisters = result.value
        // map count
          .map(x => x.count)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students with grade
        this.studentsGraded = result.value
        // filter students by grade
          .filter(x => {
            return x.examGrade != null;
          })
          // map count
          .map(x => x.count)
          // calculate sum
          .reduce((a, b) => a + b, 0);

        // get students passed
        this.studentsSuccessed = result.value
        // filter passed grades
          .filter(x => {
            return x.isPassed;
          })
          // map count
          .map(x => x.count)
          // calculate sum
          .reduce( (a, b) => a + b, 0);

        // get grade scale values
        let gradeScaleValues = [];
        // numeric grade scale
        if (gradeScale.scaleType === 0) {
          // get grade scale base
          const gradeScaleMinValue = parseInt(gradeScale.format(gradeScale.scaleBase), 10);
          // get grade scale max
          const gradeScaleMaxValue = parseInt(gradeScale.format(1), 10);
          for (let grade = gradeScaleMinValue; grade < gradeScaleMaxValue; grade++) {
            gradeScaleValues.push({
              valueFrom: gradeScale.convert(grade),
              valueTo: gradeScale.convert(grade + 1),
              name: `${grade}-${grade + 1}`,
              total: 0
            });
          }
        } else {
          gradeScaleValues = gradeScale['values'].map ( value => {
            return {
              valueFrom: value.valueFrom,
              valueTo: value.valueTo,
              name: value.name,
              total: 0
            };
          });
        }
        // get total students per value
        gradeScaleValues.forEach( value => {
          value.total = result.value.filter( x => x.examGrade >= value.valueFrom && x.examGrade < value.valueTo)
            .map(x => x.count)
            .reduce( (a, b) => a + b, 0);
        });
        // get chart data
        const _chartGradesData: any = [{
          data: gradeScaleValues.map( x => x.total ),
          label: this.translate.instant('CoursesLocal.Chart.NumberOfStudents')
        }];
        const _chartGradesLabels: any = gradeScaleValues.map( x => x.name);
        // set chart options
        this.chartGradesOptions = {
          scaleShowVerticalLines: true,
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            position: 'bottom',
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                precision: 0
              },
              scaleLabel: {
                display: true,
                labelString: this.translate.instant('CoursesLocal.Chart.NumberOfStudents')
              }
            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: this.translate.instant('CoursesLocal.Chart.Grade')
              }
            }]
          }
        };
        this.chartGradesLabels = _chartGradesLabels;
        this.chartGradesData = _chartGradesData;

        this.isLoading = false;
        this.loadingService.hideLoading();
      });
    }).catch(err => {
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });

  }

  exportGrades(courseExam) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const fileURL = this._context.getService().resolve(`instructors/me/exams/${courseExam.id}/students/export`);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        const name = courseExam.name.replace(/[/\\?%*:|"<>]/g, '-');
        a.download = `${name}-${courseExam.year.id}-${courseExam.examPeriod.name}.xlsx`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });
  }

}
