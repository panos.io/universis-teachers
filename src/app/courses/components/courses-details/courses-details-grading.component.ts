import {Component, OnInit, Input, asNativeElements, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {BsDropdownModule, BsModalService, TabsetComponent} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '../../../error/error.service';
import {ModalService} from '../../../shared/services/modal.service';
import {CoursesGradesModalComponent} from '../courses-grades-modal/courses-grades-modal.component';
import {CoursesSharedModule} from '../../courses-shared.module';
import {CoursesSharedService} from '../../services/courses-shared.service';
import {ResponseError} from '@themost/client/common';
import {LoadingService} from '../../../shared/services/loading.service';

declare var $: any;

@Component({
  selector: 'app-courses-details-grading',
  templateUrl: './courses-details-grading.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsGradingComponent implements OnInit, AfterViewInit {

  public selectedClass: any;
  public exams: any;
  public courseExams: any;

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private  coursesService: CoursesService,
              private coursesSharedService: CoursesSharedService,
              private  errorService: ErrorService,
              private router: Router,
              private route: ActivatedRoute,
              private modalService: BsModalService,
              private loadingService: LoadingService
  ) {


  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.route.parent.params.subscribe(routeParams => {
      return this.coursesService.getCourseClass(routeParams.course, routeParams.year, routeParams.period).then(courseClass => {
        if (typeof courseClass === 'undefined') {
          throw new ResponseError('Course class cannot be found', 404);
        }
        return this.coursesService.getCourseClassExams(courseClass.id).then(courseClassExams => {
          this.courseExams = courseClassExams.value;
          if (this.courseExams && this.courseExams.length > 0) {
            this.router.navigate([this.courseExams[0].id], { relativeTo: this.route });
          } else {
            this.loadingService.hideLoading();
          }
        });
      }).catch(err => {
        return this.errorService.navigateToError(err);
      });
    });

  }

  ngAfterViewInit(): void {
    //
  }
}
