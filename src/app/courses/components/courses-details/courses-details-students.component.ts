import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {BsDropdownModule} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '../../../error/error.service';
import {ProfileNotFoundError} from '../../../error/error.custom';
import {ResponseError} from '@themost/client/common';
import {LoadingService} from '../../../shared/services/loading.service';




@Component({
  selector: 'app-courses-details-students',
  templateUrl: './courses-details-students.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsStudentsComponent implements OnInit {
  public selectedClass: any;
  public exams: any;
  public courseClassStudents: any;
  public searchText = '';
  public selectedClassSection = null;
  selector = '.inner__content';
  public scrollValues: any;
  public take = 50;
  public skip = 0;

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private  coursesService: CoursesService,
              private  errorService: ErrorService,
              private loadingService: LoadingService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();

    this.route.parent.params.subscribe(routeParams => {
      this.coursesService.getCourseClass(routeParams.course, routeParams.year, routeParams.period).then(courseClass => {
        if (typeof courseClass === 'undefined') {
          return this.errorService.navigateToError(new ResponseError('Course class cannot be found or is inaccessible', 404));
        }
        // set selected course class
        this.selectedClass = courseClass;
        // get course class students
        this.coursesService.getCourseClassStudents(courseClass.id, this.skip, this.take).getItems().then( students => {
          this.courseClassStudents = students.value;
          this.scrollValues = this.courseClassStudents;
        });

        // hide loading
        this.loadingService.hideLoading();
      }).catch(err => {
        // hide loading
        this.loadingService.hideLoading();

        return this.errorService.navigateToError(err);
      });
    });
  }
getStudentsdata() {
    this.loadingService.showLoading();
    if (this.searchText.length === 0) {
      this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).getItems().then(students => {
        this.courseClassStudents = students.value;
        for (let i = 0; i < this.courseClassStudents.length; i++) {
          this.scrollValues.push(this.courseClassStudents[i]);
        }
        this.loadingService.hideLoading();
      }); } else {
        this.coursesService.searchCourseClassStudents(this.selectedClass.id, this.searchText, this.skip, this.take).getItems()
          .then(students => {
        this.courseClassStudents = students.value;

        for (let i = 0; i < this.courseClassStudents.length; i++) {
          this.scrollValues.push(this.courseClassStudents[i]);
        }
        this.loadingService.hideLoading();
      });
    }
  }

  exportStudentList(courseClass) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    let fileURL;
    if (this.selectedClassSection || this.searchText ) {
      if (this.selectedClassSection && this.searchText) {
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${courseClass.id}
        /students/export?$filter=section eq ${this.selectedClassSection}
        and (indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      } else if (this.selectedClassSection && !this.searchText) {
       // append class section query
       fileURL = this._context.getService()
         .resolve(`instructors/me/classes/${courseClass.id}/students/export?$filter=section eq ${this.selectedClassSection}`);
     } else if (this.searchText && !this.selectedClassSection) {
        // append class section query
        fileURL = this._context.getService()
          .resolve(`instructors/me/classes/${courseClass.id}
        /students/export?$filter= indexof(student/person/givenName,'${this.searchText}')ge 0
        or indexof(student/person/familyName,'${this.searchText}')ge 0
        or indexof(student/studentIdentifier,'${this.searchText}')ge 0)`);
      }
    }    else {
             fileURL = this._context.getService().resolve(`instructors/me/classes/${courseClass.id}/students/export`);
    }
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        const filtered = this.translate.instant('CoursesLocal.filtered');
        a.href = objectUrl;
        if (this.selectedClassSection || this.searchText ) {
          if (this.selectedClassSection && this.searchText) {
            a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}-${filtered}.xlsx`;
          } else if (this.selectedClassSection && !this.searchText) {
          a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${this.selectedClassSection}.xlsx`;
        } else if (this.searchText && !this.selectedClassSection) {
          a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}-${filtered}.xlsx`;
        }
       } else {
          a.download = `${courseClass.title}-${courseClass.year.id}-${courseClass.period.name}.xlsx`;
        }
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove(); // remove the element
      });

  }

  onSearchTextKeyDown($event: any) {
    if ($event.keyCode === 13) {
      this.skip = 0;
      this.loadingService.showLoading();
      // get search client data queryable
        const query = this.coursesService.searchCourseClassStudents(this.selectedClass.id, this.searchText, this.skip, this.take);
        return query.getItems().then((res) => {
          this.courseClassStudents = res.value;
          this.scrollValues = this.courseClassStudents;

          this.loadingService.hideLoading();
        }).catch( err => {
          // handle error (show in-place error)
          this.loadingService.hideLoading();
      });
    }
  }


  onSearchTextKeyUp($event: any) {

    if ($event.target && $event.target.value.length === 0) {
      // get course class student client data queryable
      const query = this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take);
      // check if a selected class section exists
      if (this.selectedClassSection) {
        // append query
        query.and('section').equal(this.selectedClassSection);
      }
      // and finally take all items
      return query.take(-1).getItems().then( students => {
        this.courseClassStudents = students.value;
      });
    }
  }

  selectChangeHandler (selected: any) {
   // show loading
   this.loadingService.showLoading();
   this.searchText = '';
   if (selected === 'null' || selected === null)   {
     return this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).getItems().then( res => {
       this.courseClassStudents = res.value;
       // hide loading
       this.loadingService.hideLoading();
   }).catch(err => {
       return this.errorService.navigateToError(err);
     });
   } else      {
       return this.coursesService.getCourseClassStudents(this.selectedClass.id, this.skip, this.take).where('section').equal(selected)
         .getItems().then(res => {
           this.courseClassStudents = res.value;
           // hide loading
           this.loadingService.hideLoading();
       }).catch(err => {
           return this.errorService.navigateToError(err);
         });
   }
 }
  onScroll() {
    this.skip += this.take;
    this.getStudentsdata();
  }
}
