import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesRecentComponent} from './components/courses-recent/courses-recent.component';
import {CoursesDetailsComponent} from './components/courses-details/courses-details.component';
import {CoursesDetailsGeneralComponent} from './components/courses-details/courses-details-general.component';
import {CoursesHistoryComponent} from './components/courses-history/courses-history.component';
import {CoursesDetailsGradingComponent} from './components/courses-details/courses-details-grading.component';
import {CoursesDetailsStudentsComponent} from './components/courses-details/courses-details-students.component';
import {CoursesDetailsExamComponent} from './components/courses-details/course-details-exam.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'recent',
        pathMatch: 'full'
      },
      {
        path: 'history',
        component: CoursesHistoryComponent
      },
      {
        path: ':course/:year/:period',
        component: CoursesDetailsComponent,
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: CoursesDetailsGeneralComponent
          },
          {
            path: 'students',
            component: CoursesDetailsStudentsComponent
          },
          {
            path: 'exams',
            component: CoursesDetailsGradingComponent,
            children: [
              {
                path: ':courseExam',
                component: CoursesDetailsExamComponent
              }
            ]
          }
        ]
      },
      {
        path: 'recent',
        component: CoursesRecentComponent
      },
      {
        path: '**',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
