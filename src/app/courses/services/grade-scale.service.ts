import { Injectable, Pipe, PipeTransform} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {round} from 'mathjs';
import {AsyncPipe, DecimalPipe} from '@angular/common';
import {ArgumentOutOfRangeError} from 'rxjs/internal/util/ArgumentOutOfRangeError';
import {ConfigurationService} from '../../shared/services/configuration.service';

export class GradeScale {

    public id: number;
    public name: string;
    public scaleType: number;
    public scaleFactor: number;
    public scaleBase: number;
    public formatPrecision = 2;
    public scalePrecision = 2;

    private _formatter: DecimalPipe;
    private readonly _decimalCharRegExp: RegExp;

    constructor(private _locale: string) {
        // init decimal pipe
        this._formatter = new DecimalPipe(this._locale);
        // get decimal separator regular expression
        this._decimalCharRegExp = new RegExp('\\' + this._formatter.transform(0.1, '1.1-1').substr(1, 1), 'ig');
    }

    /**
     * Formats the specified based on this grade scale and returns the formatted value
     * @param {number} grade
     * @returns string
     */
    format(grade: number): string {
        if (this.scaleType === 0) {
            if (typeof this.scaleFactor !== 'number') {
                throw new TypeError('Grade scale factor must be a number.');
            }
            if (this.scaleFactor <= 0) {
                throw new TypeError('Grade scale factor must greater than zero.');
            }
            // arithmetic grade scale
            if (typeof grade === 'number') {
                // get final grade by applying rounding
                const finalGrade = round((grade / this.scaleFactor), this.formatPrecision);
                // return formatted value based on the current locale
                return this._formatter.transform(finalGrade, `1.${this.formatPrecision}-${this.formatPrecision}`);
            }
            return;
        }
        throw new Error('Not yet implemented');
    }

    /**
     * Converts the given grade to the equivalent grade value base on this grade scale
     * @param grade
     */
    convert(grade: any): number {
        if (this.scaleType === 0) {
            let finalGrade;
            // if grade is a number
            if (typeof grade === 'undefined' || grade === null) {
                return;
            } else if (typeof grade === 'number') {
                finalGrade = grade;
            } else if (typeof grade === 'string') {
                // try to convert the given grade
                finalGrade = parseFloat(grade.replace(this._decimalCharRegExp, '.'));
                if (isNaN(finalGrade)) {
                    return;
                }
            }
            if (typeof this.scaleFactor !== 'number') {
                throw new TypeError('Grade scale factor must be a number.');
            }
            if (this.scaleFactor <= 0) {
                throw new TypeError('Grade scale factor must greater than zero.');
            }
            // validate grade
            const res = <number>round((finalGrade * this.scaleFactor), this.formatPrecision);
            // throw error if result is greater than 1
            if (res < 0 || res > 1) {
               throw new ArgumentOutOfRangeError();
            }
            return res;
        }
        throw new Error('Not yet implemented');
    }

}

@Injectable()
export class GradeScaleService {

    private _gradeScales: Array<GradeScale>;

    constructor(private _configurationService: ConfigurationService,
                private _context: AngularDataContext) {
        //
    }

    /**
     * Gets all the available grade scales
     */
    getGradeScales() {
        if (this._gradeScales) {
            return Promise.resolve(this._gradeScales);
        }
        return this._context.model('GradeScales').getItems().then(result => {
            const locale = this._configurationService.getCurrentLang();
           this._gradeScales = result.value.map( x => {
               return Object.assign(new GradeScale(locale), x);
           });
           return Promise.resolve(this._gradeScales);
        }, (err) => {

          console.log(err);
          return null;
        });
    }

    /**
     * Gets a grade scale based on the given identifier
     * @param {*} id
     */
    getGradeScale(id: any): Promise<GradeScale> {
        if (this._gradeScales) {
            return Promise.resolve(this._gradeScales.find(value => {
                return value.id === id;
            }));
        }
        return this.getGradeScales().then(res => {
            return Promise.resolve(res.find(value => {
                return value.id === id;
            }));
        });
    }

}

@Pipe({
    name: 'grade'
})
export class GradePipe implements PipeTransform {

    constructor(private _gradeScaleService: GradeScaleService) {
        //
    }

    transform(value: any, gradeScale?: any): any {
        if (gradeScale instanceof GradeScale) {
            return Promise.resolve(gradeScale.format(value));
        }
        return this._gradeScaleService.getGradeScale(gradeScale).then( result => {
            if (typeof result === 'undefined') {
                return Promise.reject(new Error('The specified grade scale cannot be found or is inaccessible'));
            }
            return Promise.resolve(result.format(value));
        });
    }

}
