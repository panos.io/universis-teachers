import { Injectable } from "@angular/core";
import { HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable , from, of } from 'rxjs';
import { AngularDataContext } from "@themost/angular";
import { ConfigurationService } from "../../shared/services/configuration.service";

const currentProfileProperty = 'currentProfile';
const currentUserProperty = 'currentUser';

declare var $: any;

@Injectable()
export class UserService {


  private _user: any;


  constructor(private _context: AngularDataContext,
    private _config: ConfigurationService) {
  }

  checkLogin() {
    //set refresh frame
    let settings = this._config.settings.auth;
    let _iframe = $('<iframe>', {
      src: `${settings.authorizeURL}?response_type=token&client_id=${settings.oauth2.clientID}&redirect_uri=${settings.oauth2.callbackURL}&prompt=none`,
      id: 'openid',
      frameborder: 1,
      scrolling: 'no',
      onload: function() {
        console.log('loaded!');
      }
    }).hide().appendTo('body');
  }


  /**
   * @returns {Observable<User>}
   */
  getUser() {
    if (this._user) {
      return of(this._user);
    }
    if (sessionStorage.getItem(currentUserProperty)) {
      //get user from storage
      let u = JSON.parse(sessionStorage.getItem(currentUserProperty));
      //get interactive user
      this._user = Object.assign({}, u);
      //set _context
      this._user.context = this._context;
      //set user bearer authorization
      this._context.setBearerAuthorization(this._user.token.access_token);

      //set current language
      this._context.getService().setHeader('accept-language', this._config.getCurrentLang());
      //return user
      return of(this._user);
    }
    return of(null);
  }

  setProfile(profile?: any): UserService {
    if (typeof profile === 'undefined' || profile == null) {
      sessionStorage.removeItem(currentProfileProperty);
    }
    else {
      sessionStorage.setItem(currentProfileProperty, JSON.stringify(profile));
    }
    return this;
  }

  /**
   * @returns {Observable<*>}
   */
  getProfile() {
    /*let sessionProfile = sessionStorage.getItem(currentProfileProperty);
    if (sessionProfile) {
      return of(JSON.parse(sessionProfile));
    }
    return this.getUser().flatMap((res: any) => {
      if (res) {
        this._context.setBearerAuthorization(res.token.access_token);
        return from(this._context.model('students/me')
          .asQueryable().expand('person').getItem()).map((res) => {
            this.setProfile(res);
            return res;
          });
      }
    });*/
  }

}
