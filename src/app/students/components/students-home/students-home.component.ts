import { Component, OnInit } from '@angular/core';
import {StudentsService} from '../../services/students.service';
import {forEach} from '@angular/router/src/utils/collection';
import {ErrorService} from '../../../error/error.service';


@Component({
  selector: 'app-students-home',
  templateUrl: './students-home.component.html',
  styleUrls: ['./students-home.component.scss']
})
export class StudentsHomeComponent implements OnInit {

  public studentResults; // Data
  public isLoading = true;        // Only if data is loaded
  public showDetails = false;
  public searchText = '';
  public searchStarted = false;
  public searchFinished = true;
  public allClasses: any;
  public index_arr: any;
  public info_arr: any;
  public searchProjects: any = [];
  public searchStudents: any = [];
  public student: any;
  public students: any = [];
  public allProjects: any = [];
  public isCollapsed: boolean[];


  constructor(private studentsService: StudentsService,
              private  errorService: ErrorService) {
  }


  ngOnInit() {
    this.isLoading = false;
  }

  getData(searchText: any) {
    this.searchStarted = true;
    this.searchFinished = false;
    this.studentsService.getStudentBySearch(searchText).then((res) => {
      this.searchStudents = res.value;
      this.studentsService.getCProjectsBySearch(searchText).then((res2) => {
        this.searchProjects = res2.value;
        this.isLoading = false;
        this.studentResults = this.searchStudents.concat(this.searchProjects);
        this.searchFinished = true;
        this.index_arr = removeDuplicates(this.studentResults);
        this.isCollapsed = Array(this.index_arr.length).fill(true);
        // loop through array of unique student ids
        this.students.length = 0;
        for (let i = 0; i < this.index_arr.length; i++) {
          // first find student
          this.student = this.studentResults.find(x =>
            x.student.id === this.index_arr[i]).student;
          // filter classes for this student and add to student.classes.collection
          this.student.classes = this.searchStudents.filter((x) => {
            return x.student.id === this.student.id && x.formattedGrade !== null;
          }).sort((a, b) => {
            return  a.courseClass.title < b.courseClass.title   ? -1  : 1 ;
          });
          // filter theses for this student and add to student.theses.collection
          this.student.theses = this.searchProjects.filter((x) => {
            return x.student.id === this.student.id;
          });
          // add student to array of students
          this.students.push(this.student);
        }
      }).catch(err => {
        return this.errorService.navigateToError(err);
      });
    }).catch(err => {
      return this.errorService.navigateToError(err);
    });
  }
}

function removeDuplicates(arr) {
  const unique_array = [];
  for (let i = 0; i < arr.length; i++) {
    if (unique_array.indexOf(arr[i].student.id) === -1) {
      unique_array.push(arr[i].student.id);
    }
  }
  return unique_array;
}


