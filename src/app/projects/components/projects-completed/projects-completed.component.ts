import {Component, HostListener, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-projects-completed',
  templateUrl: './projects-completed.component.html',
  styleUrls: ['./projects-completed.component.scss']
})

export class ProjectsCompletedComponent implements OnInit {
  public completedProjects: any = [];
  public isLoading = true;
  public selectedIndex = 0;
  public isSrolling = true;

  constructor( private projectsServices: ProjectsService,
               private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector("#" + tree.fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      }
    });
  }

  ngOnInit() {
    this.projectsServices.getCompletedProjects().then((res) => {
      this.completedProjects = res.value;
      this.isLoading = false;
      });
  }

  setIndex(index: number) {
    this.selectedIndex = index;
    this.isSrolling = false;
  }

  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    return this.getScroll();
  }

  public getScroll( node = document.getElementById('inner__content') ): number {

    if ( !this.isSrolling ) {
      this.isSrolling = true;
      return null;
    }

    const currentScroll = node.scrollTop;
    const elements = document.getElementsByClassName('years');
    let fountActive = false;

    this.selectedIndex = -1;

    for (let i = 0; i < elements.length; i++) {
      const currentElement = elements.item(i);

      let currentOffSetTop = currentElement.getBoundingClientRect().top;
      if (currentOffSetTop < 0) {
        currentOffSetTop *= -1;
      }

      if (((currentElement.getBoundingClientRect().top > 0) || (currentElement.getBoundingClientRect().height - 60) >
        (node.getBoundingClientRect().top + currentOffSetTop)) && !fountActive) {

        this.selectedIndex = i;
        fountActive = true;
      }
    }
    return( currentScroll );
  }

}

