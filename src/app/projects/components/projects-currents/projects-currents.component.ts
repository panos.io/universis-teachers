import { Component, OnInit } from '@angular/core';
import {ProjectsService} from '../../services/projects.service';

@Component({
  selector: 'app-projects-currents',
  templateUrl: './projects-currents.component.html',
  styleUrls: ['./projects-currents.component.scss']
})
export class ProjectsCurrentsComponent implements OnInit {
  public currentProjects: any = [];
  public isLoading = true;   // Only if data is loaded

  constructor( private projectsServices: ProjectsService) { }

  ngOnInit() {
    this.projectsServices.getCurrentProjects().then((res) => {
      this.currentProjects = res.value;
      this.isLoading = false;
    });
  }

}
