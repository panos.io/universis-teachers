import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import {LogoutComponent} from "./auth/components/logout/logout.component";
import {ErrorBaseComponent, HttpErrorComponent} from "./error/components/error-base/error-base.component";
import {AuthGuard} from "./auth/guards/auth.guard";
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LangComponent} from "./shared/lang-component";
import {ProfileModule} from './profile/profile.module';

export const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'error',
    component: ErrorBaseComponent
  },
  {
    path: 'error/:status',
    component: HttpErrorComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'lang/:id',
    component: LangComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    canActivate: [
      AuthGuard
    ],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'courses',
        loadChildren: './courses/courses.module#CoursesModule'
      },
      {
        path: 'projects',
        loadChildren: './projects/projects.module#ProjectsModule'
      },
      {
        path: 'students',
        loadChildren: './students/students.module#StudentsModule'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
