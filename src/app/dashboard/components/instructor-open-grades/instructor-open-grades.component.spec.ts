import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorOpenGradesComponent } from './instructor-open-grades.component';

describe('InstructorOpenGradesComponent', () => {
  let component: InstructorOpenGradesComponent;
  let fixture: ComponentFixture<InstructorOpenGradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorOpenGradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorOpenGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
