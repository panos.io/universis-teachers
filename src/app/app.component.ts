import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ConfigurationService} from './shared/services/configuration.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';

@Component({
    selector: 'app-root',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'app';
    constructor(private _router: Router,
                public context: AngularDataContext,
                private _config: ConfigurationService,
                public translate: TranslateService) {
        if (this._config.settings.localization && this._config.settings.localization.default) {
            translate.setDefaultLang(this._config.settings.localization.default);
        }
        else {
            translate.setDefaultLang('en');
        }
        translate.use(this._config.getCurrentLang());
    }
}
