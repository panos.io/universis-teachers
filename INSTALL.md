### universis-teachers Installation

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/)
and follow the installation instructions provided for your operating system.

## Installation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8. To install angular-cli execute

    npm install -g @angular/cli

You may need to run it as root.


Navigate to application directory and execute:

    npm i

## Development server

- Run `ng serve` for a devevelopment server
- Navigate to `http://localhost:7002/`

The app will automatically reload if you change any of the source files.

## Configuration
Copy src/config/app.json to src/config/app.production.json

Edit src/config/app.production.json:

    {
      "settings": {
        "app": {
            },
            "remote": {
                "server":"https://api.universis.io/api/"
            },
            "auth": {
                "authorizeURL":"https://users.universis.io/authorize",
                "logoutURL":"https://users.universis.io/logout?continue=http://localhost:7002/#/auth/login",
                "userProfileURL":"https://users.universis.io/me",
                "oauth2": {
                    "tokenURL": "https://users.universis.io/tokeninfo",
                    "clientID": "1219557036207244",
                    "callbackURL": "http://localhost:7002/#/auth/callback",
                    "scope": [
                        "teachers"
                    ]
                }
            },
            "localization": {
                "cultures": [ "en","el" ],
                "default": "el"
            }
        }
      }


**settings.app.remote.server**

The services endpoint of universis api server e.g. https://api.example.com/api/

**settings.app.auth.authorizeURL**

The authorization endpoint of OAuth2 Server e.g. https://users.example.com/authorize

**settings.app.auth.logoutURL**

The logout endpoint of OAuth2 Server. continue query param should point to application callback URI.

e.g. https://users.example.io/logout?continue=http://teachers.example.com/#/auth/login

**settings.app.auth.userProfileURL**

User profile endpoint of OAuth2 Server e.g. https://users.example.com/me

**settings.app.auth.oauth2.tokenURL**

The token info endpoint of OAuth2 Server e.g. https://users.example.com/tokeninfo

**settings.app.auth.oauth2.clientID**

The OAuth2 client_id that is going to be used for completing authorization flow

**settings.app.auth.oauth2.callbackURL**

The callback URI of local application e.g. https://teachers.example.com/#/auth/callback

## Docker container installation

Navigate to application root directory and build docker image:

    # build docker image
    docker build . -t universis-teachers:latest

Create docker container

    docker run -d \
      --restart always \
      --name=teachers \
      --mount source=teachers,destination=/usr/src/app/src/assets/config \
      universis-teachers:latest
